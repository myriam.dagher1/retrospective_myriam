<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Feedback extends Model
{
    use HasFactory;
    protected $hidden = ['created_at', 'updated_at'];
    public $timestamps = false;

    protected $casts = [
        'id_retrospective' => 'string',
    ];

    protected $table = 'feedbacks';


    public function retrospective():BelongsTo{

        return $this->belongsTo(Retrospective::class);
    }
}
