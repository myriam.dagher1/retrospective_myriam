<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
// use Illuminate\Foundation\Auth\User as Authenticatable ;
use Illuminate\Contracts\Auth\Authenticatable as Authenticatable;

use Illuminate\Database\Eloquent\Model ;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class User extends Model implements Authenticatable
{
    use HasFactory;

    public $table = 'utilisateurs';


    protected $hidden = ['created_at', 'updated_at'];
    public $timestamps = false;

    public function participants():BelongsToMany{

        return $this->belongsToMany(Retrospective::class, 'participants','utilisateur_id','retrospective_id');
    } 

    public function retrospectives():HasMany{

        return $this->hasMany(Retrospective::class, 'utilisateur_id');
    }

    
    public function getAuthIdentifierName()
    {
        return 'id';
    }

    public function getAuthIdentifier()
    {
        return $this->id;
    }

    public function getAuthPassword()
    {
        return $this->mdp;
    }

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

}
