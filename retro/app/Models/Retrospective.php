<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Retrospective extends Model
{
    use HasFactory;
    protected $hidden = ['created_at', 'updated_at'];

    protected $casts = [
        'id' => 'string',
    ];

    public $timestamps = false;
    
    public function participantss():BelongsToMany{

        return $this->belongsToMany(User::class, 'participants', 'retrospective_id', 'utilisateur_id');

    } 

    // public function Utilisateur

    public function feedbacks():HasMany{

        return $this->hasMany(Feedback::class, 'retrospective_id');

    }

    public function utilisateur():BelongsTo{

        return $this->belongsTo(User::class, 'utilisateur_id');
    }
}

