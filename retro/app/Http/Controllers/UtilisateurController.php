<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\View\View;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;




class UtilisateurController extends Controller
{
    /**
     * Inscription
     *
     * @param Request $request
     * @return void
     */
    public function inscription(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'prenom' => 'required|string|min:3|max:255',
            'email' => 'required|email',
            'mdp' => 'required',
        ]);

        if ($validator->fails()) {
            // dd($validator);
            return redirect()->back()->withErrors($validator->errors());
        }else{

            DB::beginTransaction();

            try {
                $inscription = new User;
                $inscription->prenom = $request->prenom;
                $inscription->email = $request->email;
                $inscription->mdp = Hash::make($request->mdp);
                $inscription->save();
    
                DB::commit();
    
                return redirect('/connexion');
    
            } catch (\Exception $e) {
    
                DB::rollBack();
                return response()->json(['message' => 'Erreur lors de la création du compte ' . $e->getMessage()]);
            }
        }
        
       
    }


    /**
     * Connexion
     *
     * @param Request $request
     * @return void
     */
    public function connexion(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'mdp' => 'required',
        ]);

        if ($validator->fails()) {
            // dd($validator);
            return redirect()->back()->withErrors($validator->errors());
        }
        else{

            $credentials = [
                'email' => $request->input('email'),
                'password' => $request->input('mdp')
            ];
    
            // dd($credentials);
    
            if (Auth::attempt($credentials)) {
                $request->session()->regenerate();
                return redirect('/moncompte');
                
            } 
    
    
            return back()->withErrors([
                'email' => 'Les informations d\'identification fournies ne correspondent pas à nos enregistrements.',
            ])->onlyInput('email');
        }
    

       
    }

    public function deconnecte()
    {
        Auth::logout(); // Déconnecte l'utilisateur

        return redirect('/connexion'); // Redirige l'utilisateur vers la page de connexion
    }

}
