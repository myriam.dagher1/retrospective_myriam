<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Retrospective;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class RetrospectiveController extends Controller
{

    /**
     * recupérer les rétrospective associées à l'utilisateur
     *
     * @return void
     */
    public function index()
    {
        $id_utilisateur = Auth::id();

        $retrospectives = Retrospective::with('utilisateur')
            ->where('utilisateur_id', $id_utilisateur)
            ->get();

        // dd($retrospectives);
        return view('mesretrospectives', compact('retrospectives'));
    }



    /**
     * récupérer la rétrospective et ses feedbacks en détails
     *
     * @param [type] $id
     * @return void
     */
    public function show($id)
    {
        $retrospective = Retrospective::find($id);
    
        if (!$retrospective) {
            return abort(404);
        }
    
        $feedbacks = $retrospective->feedbacks()->get();

        $participe = User::with(['participants' => function ($query) use ($id) {
            $query->wherePivot('retrospective_id', $id);
        }])->find(Auth::id());
    
        return view('retrospective', compact('retrospective', 'feedbacks', 'participe'));
    }
    

    /**
     * Afficher une retro grace à sa clé
     *
     * @param Request $request
     * @return void
     */
    public function showWithKey(Request $request)
    {
        $retrospective = Retrospective::find($request->id);
    
        if (!$retrospective) {
            return abort(404);
        }
    
        $feedbacks = $retrospective->feedbacks()->get();
        
        $retroId = $request->id;

        $participe = User::with(['participants' => function ($query) use ($retroId) {
            $query->wherePivot('retrospective_id', $retroId);
        }])->find(Auth::id());

        return view('retrospective', compact('retrospective', 'participe'));
    }



    /**
     * Creation d'une rétrospective
     *
     * @param Request $request
     * @return void
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'titre' => 'required|string|min:3|max:255',
            'dateDebut' => 'required|date|after_or_equal:today',
            'dateFin' => 'required|date|after_or_equal:dateDebut',
        ]);

        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors());
        } else {

            try {

                db::beginTransaction();
                $id_utilisateur = Auth::id();
                $id = Str::uuid();


                $retro = new Retrospective();
                $retro->id = $id;
                $retro->utilisateur_id = $id_utilisateur;
                $retro->titre = $request->titre;
                $retro->date_debut = $request->dateDebut;
                $retro->date_fin = $request->dateFin;
                // dd($retro);

                $retro->save();
                DB::commit();
                return redirect('/mesretrospectives');
                
            } catch (\Exception $e) {

                DB::rollBack();

                return response()->json(['message' => 'Erreur lors de la création de la rétrospective ' . $e->getMessage()]);
            }
        }
    }
}
