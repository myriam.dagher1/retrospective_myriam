<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Feedback;
use App\Models\Retrospective;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;



class FeedbackController extends Controller
{
    public function store(Request $request, $id)
    {

        $validator = Validator::make($request->all(), [
            'contenus' => 'required|string|min:3',
            'categorie' => 'required|min:1|max:1',
        ]);

        // $dataRetro = Retrospective::find($id);
        // compact('dataRetro');

        // $date_fin = $dataRetro->date_fin;



        if ($validator->fails()) {

            return redirect()->back()->withErrors($validator->errors());
        } else {
            try {
                DB::beginTransaction();

                $feedback = new Feedback();
                $feedback->retrospective_id = $id;
                $feedback->contenus = $request->contenus;
                $feedback->categorie = $request->categorie;
                $feedback->date_creation = now();
                $feedback->save();

                $retrospective = Retrospective::find($id);
                $retrospective->participantss()->attach(Auth::id());


                DB::commit();

                return redirect()->route('page-feedback', ['id' => $id])->with('success', 'Le feedback a été créé avec succès !');
            } catch (\Exception $e) {

                DB::rollBack();
                return response()->json(['message' => 'Erreur lors de la création du/des feedback ' . $e->getMessage()]);
            }
        }
    }
}

