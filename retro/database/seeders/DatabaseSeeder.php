<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\Feedback;
use App\Models\Participant;
use App\Models\User;
use App\Models\Retrospective;
use Illuminate\Database\Seeder;
use Database\Factories\FeedbackFactory;
use Database\Factories\RetrospectiveFactory;
use Database\Factories\ParticipantFactory;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        User::factory()->count(10)->create();
        Retrospective::factory()->count(10)->create();
        Feedback::factory()->count(10)->create();
        Participant::factory()->count(10)->create();

        
    }
}