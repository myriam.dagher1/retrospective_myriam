<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Retrospective;
use App\Models\User;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ParticipantFactory extends Factory
{

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $retrospectiveIds = Retrospective::pluck('id')->toArray();        
        $utilisateurIds = User::pluck('id')->toArray();

        return [

            'retrospective_id' => $this->faker->randomElement($retrospectiveIds),
            'utilisateur_id' => $this->faker->randomElement($utilisateurIds),

        ];
    }
}


