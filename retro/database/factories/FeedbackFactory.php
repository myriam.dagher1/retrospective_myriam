<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Retrospective;
use App\Models\Feedback;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Feedback>
 */
class FeedbackFactory extends Factory
{

    protected $model = Feedback::class;

    /**
     * Données de test de la table retrospective pour insertion dans BDD
     *
     * @return array
     */
    public function definition(): array
    {
        $retrospectiveIds = Retrospective::pluck('id')->toArray();        

        return [

            'retrospective_id' => $this->faker->randomElement($retrospectiveIds),
            'categorie' => $this->faker->numberBetween(0,1),
            'contenus' => $this->faker->sentence(),
            'date_creation' => now(),
        ];
    }
}
