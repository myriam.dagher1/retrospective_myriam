<?php

namespace Database\Factories;

use App\Models\Retrospective;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;

class RetrospectiveFactory extends Factory
{
    protected $model = Retrospective::class;
    
    /**
     * Données de test de la table utilisateurs pour insertion en BDD
     *
     * @return void
     */
    public function definition()
    {
        $utilisateurIds = User::pluck('id')->toArray();
        return [
            'id' => $this->faker->uuid(),
            'utilisateur_id' => $this->faker->randomElement($utilisateurIds),
            'titre' => $this->faker->sentence(),
            'date_debut' => now(),
            'date_fin' => $this->faker->dateTimeBetween($startDate = 'now', $endDate = '+30 days'),
        ];
    }
}
