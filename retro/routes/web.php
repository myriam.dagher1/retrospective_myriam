<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UtilisateurController;
use App\Http\Controllers\RetrospectiveController;
use App\Http\Controllers\FeedbackController;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/', function () {
    return view('accueil');
});

Route::get('/inscription', function () {
    return view('inscription');
});

Route::post('/inscription', [UtilisateurController::class, 'inscription']);


Route::get('/connexion', function () {
    return view('connexion');

})->name('connexion');

Route::post('/connexion', [UtilisateurController::class, 'connexion']);

Route::post('/logout', [UtilisateurController::class, 'deconnecte']);


Route::get('/moncompte', function () { // interface utilisateur connecté
    return view('accueilConnect');

})->middleware('auth');

Route::post('/creerRetro', [RetrospectiveController::class, 'store'])->middleware('auth'); //créer une retro

Route::get('/retro/{id}/creerfeedback', function ($id) { // interface utilisateur connecté
    
    return view('feedbacks', compact('id'));

})->name('page-feedback')->middleware('auth');


Route::post('/retro/{id}/creerfeedback', [FeedbackController::class, 'store'])->name('feedbacks.store');


Route::get('/mesretrospectives', [RetrospectiveController::class, 'index'])->middleware('auth'); //affiche les rétrospective crée par l'utilisateur connecté

Route::get('/mesretrospectives/{id}', [RetrospectiveController::class, 'show'])->name('retro.show')->middleware('auth'); //Affiche une retro en particulier

Route::get('/joinRetro', [RetrospectiveController::class, 'showWithKey'])->name('retro.key')->middleware('auth'); // Affiche une rétrospective en particulier


