<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="style_co.css">
    <title>Connecter</title>
</head>

<body>

    <section id="box">
        <h1>Se connecter</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <form action="{{url('connexion')}}" method="post">

            @csrf

            <label for="email">Email</label>
            <input type="text" name="email" id="">

            <label for="">Mot de passe</label>
            <input type="password" name="mdp" id="">

            <div class="btn1">
                <input type="submit" id="button" value="Se connecter">
            </div>
            <a href="{{url('inscription')}}">S'inscrire</a>

        </form>
    </section>

</body>

</html>