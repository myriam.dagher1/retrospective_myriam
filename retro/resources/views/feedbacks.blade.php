@extends('layouts.header')

@section('title', 'Créer un feedback')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/style_cr.css') }}">
@endsection

@section('menu', '🍟')

@section('linkmenu')
<a id="linkmenu" href="{{url('moncompte')}}">Mon compte</a>
@endsection
@section('linkmenu2')
<a id="linkmenu" href="{{url('mesretrospectives')}}">Mes retrospectives</a>
@endsection

@section('content')

<h1>Créer un feedback</h1>


<form action="{{ route('feedbacks.store', $id)}}" method="post" id="feedback-form">
    @csrf

    <label for="contenus">Contenus :</label>
    <input class="inputconf" type="textarea" name="contenus" placeholder="Mon feedback" id="contenus">

    <label for="categorie">Categorie :</label>
    <select class="inputconf" name="categorie" id="categorie">
        <option value="">choisissez une catégorie</option>
        <option value="1">J'aime</option>
        <option value="0">J'aime pas</option>
    </select>

    <div class="modale" id="feedbacks">
        <p class="question">Voulez-vous confirmer la création du feedback ?</p>
        <a id="closefeedback2">Annuler</a>
        <input id="btnsubmit" type="submit" value="Confirmer">
    </div>
</form>

<button id="btn1" >Envoyer</button>

@endsection