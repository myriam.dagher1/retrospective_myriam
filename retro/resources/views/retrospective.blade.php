@extends('layouts.header')

@section('title', '{{ $retrospective->titre }}')

@section('styles')
<link rel="stylesheet" href="{{ asset('/style_retro.css') }}">
@endsection

@section('menu', '🌭')

@section('linkmenu')
<a id="linkmenu" href="{{ url('mesretrospectives') }}">Mes retrospectives</a>
@endsection
@section('linkmenu2')
<a id="linkmenu" href="{{ url('moncompte') }}">Mon compte</a>
@endsection

@section('content')


<section id="box">
    <h1>{{ $retrospective->titre }}</h1>
    <p>Date de début : {{ $retrospective->date_debut }}</p>
    <p>Date de fin : {{ $retrospective->date_fin }}</p>


    @if ($retrospective->date_fin < now()) @php $error='La rétro est terminée.' ; @endphp @endif @if (isset($error)) <p>
        {{ $error }}</p>
        @endif

        @if (
        $retrospective->utilisateur_id == Auth::id() || count($participe->participants) > 1 ) <h2>Feedbacks</h2>

            @if (count($retrospective->feedbacks) == 0)
            <p>Il n'y a pas de feedback disponibles !</p>
            @else
            <section id="aime">
                <p>J'aime</p>

                <ul>

                    @foreach ($retrospective->feedbacks as $feedback)
                    @if ($feedback->categorie == '1')
                    <li>{{ $feedback->contenus }}</li>
                    @endif
                    @endforeach
                </ul>
            </section>


            <section id="aimepas">
                <p>J'aime pas</p>

                <ul>
                    @foreach ($retrospective->feedbacks as $feedback)
                    @if ($feedback->categorie == '0')
                    <li>{{ $feedback->contenus }}</li>
                    @endif
                    @endforeach
                </ul>
                @endif
                @else
                <p>Creer au moins deux feedback</p>
                @endif
            </section>


            @if (!isset($error))
            <a href="{{ route('page-feedback', $retrospective->id) }}">Creer un feedback</a>
            @endif

            <a href="{{ url('/mesretrospectives') }}">Afficher mes retrospectives</a>
</section>


@endsection