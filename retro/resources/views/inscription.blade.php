<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="style_co.css">
    <title>Inscription</title>
</head>

<body>



    <section id="box">
        <h1>S'inscrire</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <form name="s_inscrire" method="post" action="{{url('inscription')}}">

            @csrf
            <label for="prenom">Nom :</label>
            <input type="text" id="title" name="prenom" required="">

            <label for="email">Email :</label>
            <input type="text" name="email" required>

            <label for="mdp">Mot de passe</label>
            <input type="password" name="mdp" id="" required>

            <div class="btn1">

                <input id="button" type="submit" value="Créer">

            </div>
        </form>
        <a href="{{url('connexion')}}">J'ai déjà un compte</a>

    </section>

</body>

</html>