@extends('layouts.header')

@section('title', 'Mon compte')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/style_connected.css') }}">
@endsection

@section('menu', '🥙')

@section('linkmenu')
<a id="linkmenu" href="{{url('mesretrospectives')}}">Mes retrospectives</a>
@endsection

@section('content')

@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>

@endif

<section id="box">
    <section id="boxcreer">
        <h1>Créer Rétrospective</h1>

        <form name="retro" method="post" action="{{url('creerRetro')}}">

            @csrf
            <label for="prenom">Titre</label>
            <input type="text" id="title" name="titre" required="", placeholder="Ma retrospective">

            <label for="email">Date de début</label>
            <input type="date" name="dateDebut" min="{{ date('Y-m-d') }}">

            <label for="mdp">Date de fin</label>
            <input type="date" name="dateFin" id="" min="{{ date('Y-m-d', strtotime('+1 day')) }}">

            <div class="button">
                <input type="submit" class="btn" value="Créer">

            </div>

        </form>

    </section>

    <section id="boxaccess">
        <h1>Faire un feedback</h1>

        <form action="{{ route('retro.key') }}" method="get">


            @csrf
            <label for="id">Clé de rétrospective</label>
            <input type="text" name="id" id="id" placeholder="fcd215c7-27a7-3736-a737-57a3f6e657b9">

            <div class="button">
                <input type="submit" class="btn key" value="Envoyer">
            </div>
        </form>
    </section>



</section>
@endsection