@extends('layouts.header')

@section('title', 'Mes retrospectives')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/style_mesretros.css') }}">
@endsection

@section('menu', '🍔')

@section('linkmenu')
<a id="linkmenu" href="{{url('moncompte')}}">Mon compte</a>
@endsection

@section('content')
<h1>Mes rétrospectives :</h1>

<section id="box">
    @if(count($retrospectives) > 0)
    @foreach($retrospectives as $retrospective)
    <div class="truc3">
        <div class="truc2">@if($retrospective->date_fin > now()) <p> En cours</p> @else <p> Terminé</p> @endif</div>

        <a href="{{ route('retro.show', $retrospective->id) }}">
            <div class="truc">
                <p>{{ $retrospective->titre }}</p>
            </div>
        </a>
    </div>



    @endforeach
    @else
    <p>Vous n'avez pas créer de rétro</p>
    @endif
</section>

@endsection
<!-- <a href="{{url('/moncompte')}}">mon compte</a> -->