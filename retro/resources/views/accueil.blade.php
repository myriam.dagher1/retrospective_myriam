<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="/style_acc.css">
    <title>Accueil</title>
</head>

<body>

    <section class="intro">
        <h1>RETRO</h1>
        <p>
            Bienvenue dans notre application de rétro, un outil puissant conçu pour vous aider à tirer le meilleur parti de vos expériences passées et à les utiliser pour améliorer votre avenir. Avec notre application, vous pouvez réfléchir sur les événements passés, identifier les domaines dans lesquels vous avez réussi et ceux dans lesquels vous pourriez vous améliorer, et élaborer des stratégies pour vous aider à atteindre vos objectifs futurs. 
        </p>
    </section>

    <section class="link">
        <a class="link1" href="{{url('connexion')}}">Se connecter</a>
        <a class="link2" href="{{url('inscription')}}">S'inscrire</a>
    </section>
    </section>

</body>

</html>