<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>@yield('titre')</title>
    <link rel="stylesheet" href="{{ asset('/styles_menu.css') }}">
    @yield('styles')
    <script src="{{ asset('/script.js') }}" defer></script>
</head>
<body>

<!-- Bouton de menu -->
<button id="menu" onclick="addclass()">@yield('menu')</button>

<!-- Fenêtre modale -->
<section id="abcd" class="window">

    <!-- Liste des liens -->
    <div class="liste">
        <button id="exit" onclick="removeclass()">❌</button>
        @yield('linkmenu')
        @yield('linkmenu2')

    </div>

    <!-- Formulaire de déconnexion -->
    <form action="{{ url('logout') }}" method="POST">
        @csrf
        <button id="deco" type="submit" class="btn btn-primary">Déconnexion</button>
    </form>

</section>

<!-- Contenu de la page -->
    @yield('content')

</body>
</html>
