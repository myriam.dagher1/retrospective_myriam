# Table des matières
1. [Intitulé de l'application](#intitulé)
2. [Les technos utilisés](#technos)
3. [UML](#uml)
4. [Base de données](#bdd)
---
## Intitulé de l'application <a name=intitulé></a>

La rétrospective est une des dernières étapes d'un sprint Scrum. C'est l'occasion de recueillir les retours des membres de l'équipe.
Pour faire cela, nous souhaitons mettre en place une application simple permettant aux membres d'une équipe de développement de saisir des retours, positifs/négatifs de manière anonyme en préambule préparation de la rétrospective.

## Les technos utilisés : <a name=technos></a>

Laravel : fullstack  
Sql : Base de données  
Figma : Maquette, Lien de la maquette :   
https://www.figma.com/file/KUaQ3pDkoEqOEJZoNmyifg/retro?t=OckDPItSJBUtg2BJ-1   
sinon un pdf de la maquette est disponible.

## UML : <a name=uml></a>

Un utilisateur anonyme peut s'inscrire et se connecter, une fois l'utilisateur connecté il peut ajouter des feedback à une rétrospective dans les categories "j'aime" et "je n'aime pas". L'utilisateur peut créer une rétrospective ce qui fera de lui l'administrateur de la rétrospective, il pourra alors consulter les feedback ainsi que les utilisateurs n'ayant pas encore participé uniquement lorsque les utilisateurs participant depassent la moitié des participants requis, il lui sera aussi possible de supprimer la rétrospective.

---

1. UML: diagramme de cas d'utilisation

<img src="UML/use_case/use_case_feedbackApp.svg"/>


2. UML : diagramme d'activité

- Participant à une rétrospective

<img src="UML/diagramme_activité/diag_activite_creation_feedback_.png"/>

- Administrateur d'une rétrospective

<img src="UML/diagramme_activité/diag_activité_gestion_retro_.png"/>

3. UML : diagramme de classe

<img src="UML/diagramme_de_classe/diag_classe.jpg"/>

---

## Base de données : <a name=bdd></a>

1. Modéle Conceptuel de Données

<img src="base_de_donnees/mcd/mcd_.svg" />

2. Modèle Logique de Données

<img src="base_de_donnees/mld/mld_.png" />