CREATE DATABASE retro;

CREATE USER IF NOT EXISTS 'userRetro'@'localhost' IDENTIFIED BY 'password';

GRANT ALL PRIVILEGES ON retro.* TO 'userRetro'@'localhost';

FLUSH PRIVILEGES;

USE retro; 

CREATE TABLE `utilisateurs` (
  `id` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `prenom` varchar(255) ,
  `email` varchar(255) ,
  `mdp` varchar(255) 
);

CREATE TABLE `retrospectives` (
  `id` varchar(50) NOT NULL PRIMARY KEY ,
  `utilisateur_id` int,
  `titre` varchar(255),
  `date_debut` date,
  `date_fin` date,
  FOREIGN KEY (utilisateur_id) REFERENCES utilisateurs(id) ON DELETE CASCADE ON UPDATE CASCADE 
);

CREATE TABLE `feedbacks` (
  `id` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `retrospective_id` varchar(50),
  `categorie` boolean,
  `contenus` text,
  `date_creation` date,
  FOREIGN KEY (retrospective_id) REFERENCES retrospectives(id) ON DELETE CASCADE ON UPDATE CASCADE 
);

CREATE TABLE `participants` (
  `utilisateur_id` int NOT NULL PRIMARY KEY,
  `retrospective_id` varchar(50) NOT NULL,
  `date_creation` date,
  FOREIGN KEY (utilisateur_id) REFERENCES utilisateurs(id) ON DELETE CASCADE ON UPDATE CASCADE ,
  FOREIGN KEY (retrospective_id) REFERENCES retrospectives(id) ON DELETE CASCADE ON UPDATE CASCADE 
);
