# Modèle Logique de données (dbdiagram)

```

Table utilisateurs {
  id int [pk]
  prenom varchar
  email varchar
  mdp varchar
}

Table retrospectives {
  id int [pk]
  utilisateur_id int
  titre varchar
  date_debut date
  date_fin date
}

Table participants {
  utilisateur_id int [pk]
  retrospective_id int [pk]
 }

Table feedbacks {
  id int [pk]
  retrospective_id int
  contenus text
  categorie boolean
  date_creation date

 }


Ref: utilisateurs.id > retrospectives.utilisateur_id
Ref: utilisateurs.id > participants.utilisateur_id
Ref: retrospectives.id > participants.retrospective_id
Ref: retrospectives.id < feedbacks.retrospective_id


```

